--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import Hakyll
import Data.Monoid ((<>))

config :: Configuration
config = defaultConfiguration {
  destinationDirectory = "public"
}

--------------------------------------------------------------------------------

main :: IO ()
main = hakyllWith config $ do
  match "images/*" $ do
    route idRoute
    compile copyFileCompiler

  match "css/*" $ do
    route idRoute
    compile compressCssCompiler

  match (fromList ["about.rst"]) $ do
    route $ setExtension "html"
    compile $ pandocCompiler >>= applyDefault siteCtx

  match "posts/*" $ do
    route $ setExtension "html"
    compile $ pandocCompiler
      >>= loadAndApplyTemplate "templates/post.html" postCtx
      >>= applyDefault postCtx

  create ["archive.html"] $ do
    route idRoute
    compile $ do
      posts <- recentFirst =<< loadAll "posts/*"
      let archiveCtx =
            listField "posts" postCtx (return posts)
            <> constField "title" "Archives"
            <> siteCtx
      makeItem ""
        >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
        >>= applyDefault archiveCtx

  match "index.html" $ do
    route idRoute
    compile $ do
      posts <- recentFirst =<< loadAll "posts/*"
      let indexCtx =
            listField "posts" postCtx (return posts)
            <> constField "title" "Home"
            <> siteCtx
      getResourceBody
        >>= applyAsTemplate indexCtx
        >>= applyDefault indexCtx

  match "templates/*" $ compile templateCompiler


--------------------------------------------------------------------------------

applyDefault ctx =
  (=<<) relativizeUrls .
  loadAndApplyTemplate "templates/default.html" ctx

--------------------------------------------------------------------------------

postCtx :: Context String
postCtx =
  dateField "date" "%B %e, %Y"
  <> siteCtx

siteCtx :: Context String
siteCtx =
  activeClassField
  <> defaultContext

activeClassField :: Context a
activeClassField = functionField "activeClass" $ \[p] _ -> do
  path <- fmap toFilePath getUnderlying
  return $ if path == p then "active" else path
